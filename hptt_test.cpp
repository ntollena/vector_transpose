#include <hptt.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>   

#define DIM 6

long get_ns() {
  struct timespec ts;
  clock_gettime(CLOCK_MONOTONIC, &ts);
  return (long)ts.tv_sec * 1000000000 + ts.tv_nsec;
}


long many_try(float* mat, float* matT, void (*transpose)(float*, float*), int rep) {
  long dtime;
  dtime = get_ns();
  for (int i = 0; i < rep; i++) {
    (*transpose)(mat, matT);
  }
  dtime = get_ns() - dtime;
  dtime /= rep;
  printf("Transpose took on average %ld nanoseconds, %d repetitions \n", dtime, rep);
  return dtime;
}

int main() {

    // allocate tensors
    float *A = (float *)malloc(sizeof(float) * 48 * 28 * 48 * 28 * 48 * 28);
    float *B = (float *)malloc(sizeof(float) * 48 * 28 * 48 * 28 * 48 * 28);

    // specify permutation and size
    int dim = 6;
    int perm[DIM] = {5,2,0,4,1,3};
    int size[DIM] = {48,28,48,28,48, 28};
    float alpha = 1.;
    float beta = 1.;
    int numThreads = 8;

    // create a plan (shared_ptr)
    auto plan = hptt::create_plan( perm, dim, 
                                   alpha, A, size, NULL, 
                                   beta,  B, NULL, 
                                   hptt::ESTIMATE, numThreads);

    // execute the transposition
    plan->execute();

}
