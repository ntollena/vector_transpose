#include <stdio.h>
#include <x86intrin.h>
#include <string.h>   
#include <time.h>   
#include "utils.h"
#include "micro_kernels.h"
#include <assert.h>

#undef USE_VEC_SSE
#undef USE_VEC_AVX
#undef USE_MEMCPY
#undef USE_NAIVE
#define TRANSPOSE

#define LOOP_OVER(function_name, block_call /*void block_call(float*, float*, int i, int j)*/) \
void function_name(float* __restrict__ mat, float* __restrict__ matT){\
  for (int i = 0; i < MAT_SIZE; i += BLOCK_SIZE) {\
    for (int j = 0; j < MAT_SIZE; j += BLOCK_SIZE) {\
      block_call(mat, matT, i, j);\
    }\
  }\
}
#define UNROLL_J_LOOP(block_call, mat, matT, i) \
    for (int j = 0; j < MAT_SIZE; j += BLOCK_SIZE * 4) {\
      block_call(mat, matT, (i), j);\
      block_call(mat, matT, (i), j + BLOCK_SIZE);\
      block_call(mat, matT, (i), j + 2 * BLOCK_SIZE);\
      block_call(mat, matT, (i), j + 3 * BLOCK_SIZE);\
    }

#define UNROLL_4_OVER(function_name, block_call/*void block_call(float*, float*, int i, int j)*/) \
void function_name(float* __restrict__ mat, float* __restrict__ matT){\
  for (int i = 0; i < MAT_SIZE; i += BLOCK_SIZE) {\
        UNROLL_J_LOOP(block_call, mat, matT, i);\
    }\
}

//LOOP_OVER(transpose_blocks_naive, copy_naive_out_bxb)
UNROLL_4_OVER(transpose_blocks_naive, copy_naive_out_bxb)
//LOOP_OVER(transpose_blocks_sse_4x4, copy_sse_4x4)
UNROLL_4_OVER(transpose_blocks_sse_4x4, copy_sse_4x4)

#if HAS_AVX
//LOOP_OVER(transpose_blocks_avx_8x8, copy_avx_8x8)
UNROLL_4_OVER(transpose_blocks_avx_8x8, copy_avx_8x8)
//LOOP_OVER(copy_all_avx_8x8, copy_contiguous_avx_8x8)
UNROLL_4_OVER(copy_all_avx_8x8, copy_contiguous_avx_8x8)
//LOOP_OVER(real_transpose, trans_unpack_out_8x8)
UNROLL_4_OVER(real_transpose, trans_unpack_out_8x8)
#endif
long NBYTES = MAT_SIZE * MAT_SIZE * sizeof(float);

void copy_all(float* __restrict__ mat, float* __restrict__ matT) {
  memcpy(matT, mat, NBYTES);
}


void mesure_throughput(float* __restrict__ mat, float* __restrict__ matT, 
    void (kernel)(float* __restrict__, float* __restrict__)){
    __int128 result;
    float throughput;
    int rep = MAX((1 << 20) / MAT_SIZE, 10);
    printf("REP: %d\n", rep);
    result = many_try_out_tuple(mat, matT, kernel, rep);
    throughput =  NBYTES / ((float) (unsigned long long)(result >> 64));
    printf("Estimated throughput = %f Gbytes / sec\n", throughput);
    throughput = NBYTES / ((float) (unsigned long long)result);
    printf("Estimated throughput = %f bytes / cycle\n", throughput);
}

int main(void) {
    int i;
    float *mat, *matT;
    mat = (float*) aligned_alloc(16 * sizeof(float), sizeof(float) * MAT_SIZE * MAT_SIZE);
    matT = (float*) aligned_alloc(16 * sizeof(float), sizeof(float) * MAT_SIZE * MAT_SIZE);

    for(i=0; i<MAT_SIZE * MAT_SIZE; i++) mat[i] = i;
    printf("Moving %d * %d float = %ld bytes by blocks of %d * %d float = %ld bytes\n", 
        MAT_SIZE , MAT_SIZE, NBYTES, BLOCK_SIZE , BLOCK_SIZE, BLOCK_SIZE * BLOCK_SIZE * sizeof(float));

    printf("NAIVE BLOCK COPY\n");
    mesure_throughput(mat, matT, transpose_blocks_naive);
#if HAS_AVX
#ifdef TRANSPOSE
    printf("\nTRANSPOSE\n");
    mesure_throughput(mat, matT, real_transpose);
#endif
    printf("\nAvx block copy\n");
    mesure_throughput(mat, matT, transpose_blocks_avx_8x8);
    printf("\nAvx full copy\n");
    mesure_throughput(mat, matT, copy_all_avx_8x8);
#endif
    printf("\nMemcpy full copy\n");
    mesure_throughput(mat, matT, copy_all);
    //assert(verify(mat, matT, MAT_SIZE));
    printf("\nSSE transpose\n");
    mesure_throughput(mat, matT, sse_transpose);
    free(mat);
    free(matT);
    return 0;
}
