CC=gcc
BUILD=./build
FLAGS=-Wall -O2 -march=core-avx2
LFLAGS=-lm
SRC=transpose_blocks.c utils.c
OBJ= $(SRC:.c=.o)

exec: build 
	$(BUILD)/transpose_blocks.exe

build:$(BUILD)/transpose_blocks.exe

$(BUILD)/transpose_blocks.exe:$(patsubst %.o, $(BUILD)/%.o,$(OBJ))
	$(CC) $(FLAGS) -o $@ $^ $(LFLAGS)

$(BUILD)/%.o:%.c micro_kernels.h utils.h
	$(CC) $(FLAGS) -o $@ -c $< $(LFLAGS)

clean: 
	rm ${BUILD}/*.o ${BUILD}/*.exe
