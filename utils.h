#ifndef UTILS_H
#define UTILS_H
#include <stdio.h>
#include <x86intrin.h>
#include <string.h>   
#include <time.h>   

#define BLOCK_SIZE 8
#define MAT_SIZE (1 << 6)

#define HAS_AVX 1

#define MAX(a,b) ((a) >= (b)?(a):(b))

int verify(float* mat, float* matT, int n);
long get_ns();
unsigned long get_cycle();
__int128 many_try_out_tuple(float* mat, float* __restrict__ matT,  
    void (*transpose)(float*, float*), int rep); 
long many_try_out_cycle(float* __restrict__ mat, float* __restrict__ matT, 
    void (*transpose)(float*, float *), int rep);
long many_try(float* mat, void (*transpose)(float*), int rep);
long many_try_out(float* __restrict__ mat, float* __restrict__ matT, 
    void (*transpose)(float*, float *), int rep);
#endif
