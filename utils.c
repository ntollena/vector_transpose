#include <time.h>
#include <stdio.h>
#include <assert.h>
#define MAT_SIZE (1 << 6)

long get_ns() {
  struct timespec ts;
  clock_gettime(CLOCK_MONOTONIC, &ts);
  return (long)ts.tv_sec * 1000000000 + ts.tv_nsec;
}

int verify(float* mat, float* matT, int n) {
  for (int i = 0; i < n; i++){
    for (int j = 0; j < n; j++){
      if( mat[i * n + j] != matT[j * n + i]) {
        printf("failed at i, j = %d, %d; mat[%d][%d] = %f, matT[%d][%d] = %f\n", 
            i, j, i, j, mat[i * n + j], j, i, matT[j * n + i]);
        return 0;}
    }
  }
  return 1;
}

unsigned long get_cycle() {
  unsigned long result ;
  unsigned long result2;
  asm volatile("rdtsc":"=a" (result), "=d" (result2));
  return ((unsigned long long)result2 << 32) + result;
}

__int128 many_try_out_tuple(float* mat, float* __restrict__ matT,  
    void (*transpose)(float* __restrict__, float* __restrict__), int rep) {
  unsigned long dtime, dtime_ns;
  dtime_ns = get_ns();
  dtime = get_cycle();
  for (int i = 0; i < rep; i++) {
    (*transpose)(mat, matT);
  }
  dtime = get_cycle() - dtime;
  dtime_ns = get_ns() - dtime_ns;
  dtime /= rep;
  dtime_ns /= rep;
  return ((__int128) dtime_ns << 64) + dtime;
}

long many_try_out_cycle(float* __restrict__ mat, float* __restrict__ matT, void (*transpose)(float*, float *), int rep) {
  long dtime, dtime2;
  dtime = get_cycle();
  for (int i = 0; i < rep; i++) {
    (*transpose)(mat, matT);
  }
  dtime2 = get_cycle() - dtime;
  dtime2 /= rep;
  printf("Transpose took on average %ld cycles, %d repetitions \n", dtime2, rep);
  return dtime2;
}

__int128 many_try_tuple(float* mat, void (*transpose)(float*), int rep) {
  unsigned long dtime, dtime_ns;
  dtime = get_cycle();
  dtime_ns = get_ns();
  for (int i = 0; i < rep; i++) {
    (*transpose)(mat);
  }
  dtime_ns = get_ns() -get_ns();
  dtime = get_cycle() - dtime;
  dtime /= rep;
  printf("Transpose took on average %ld cycle, %d repetitions \n", dtime, rep);
  return ((__int128) dtime_ns << 64) + dtime;
}

long many_try(float* mat, void (*transpose)(float*), int rep) {
  unsigned long dtime;
  dtime = get_ns();
  for (int i = 0; i < rep; i++) {
    (*transpose)(mat);
  }
  dtime = get_ns() - dtime;
  dtime /= rep;
  printf("Transpose took on average %ld nanoseconds, %d repetitions \n", dtime, rep);
  return dtime;
}

long many_try_out(float* __restrict__ mat, float* __restrict__ matT, void (*transpose)(float*, float *), int rep) {
  long dtime, dtime2;
  dtime = get_ns();
  for (int i = 0; i < rep; i++) {
    (*transpose)(mat, matT);
  }
  dtime2 = get_ns() - dtime;
  dtime2 /= rep;
  printf("Transpose took on average %ld ns, %d repetitions \n", dtime2, rep);
  return dtime2;
}

