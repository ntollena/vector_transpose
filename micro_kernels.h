#ifndef MICRO_KERNEL_H
#define MICRO_KERNEL_H
#include "utils.h"

// For every function, we assume load from block (i, j) and store to 
// block (j, i)
__attribute__((always_inline)) void copy_sse_4x4(float* __restrict__, float* __restrict__, int i, int j);
__attribute__((always_inline)) void trans_naive_out_bxb(float* __restrict__, float* __restrict__, int i, int j);
__attribute__((always_inline)) void copy_naive_out_bxb(float* __restrict__, float* __restrict__, int i, int j);
__attribute__((always_inline)) void trans_unpack_out_4x4(float* __restrict__ mat, float* __restrict__ matT, int i, int j);
__attribute__((always_inline)) void trans_unpack_out_4x4_size(float* __restrict__, float* __restrict__, int i, int j, int size);
#ifdef HAS_AVX
__attribute__((always_inline)) void trans_unpack_out_8x8(float* __restrict__, float* __restrict__, int i, int j);
__attribute__((always_inline)) void trans_unpack_out_8x8_size(float* __restrict__, float* __restrict__, int i, int j, int size);
__attribute__((always_inline)) void trans_blend_out_8x8(float* __restrict__, float* __restrict__, int i, int j);
__attribute__((always_inline)) void copy_avx_8x8(float* __restrict__, float* __restrict__, int i, int j);
__attribute__((always_inline)) void copy_contiguous_avx_8x8(float* __restrict__, float* __restrict__, int i, int j);
#endif

#include "micro_kernels.c"
#endif
