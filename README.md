# Project description

This project aims to evaluate possible strategies for doing a transposition of
small matrices in cpu. Matrices are 8x8 (not more) for the moment.

# Sources
 
https://stackoverflow.com/questions/25622745/transpose-an-8x8-float-using-avx-avx2
Apparently blend can be used for reducing pressure on port 5 - port on which
vector shuffles are mapped. Benefit is not clear and seems to be DEEPLY hardware
dependent.
https://stackoverflow.com/questions/29519222/how-to-transpose-a-16x16-matrix-using-simd-instructions

# Remarks

An ideal transposition read and write exactly once all elements of a N x N
matrix except for the diagonal (in practice, diagonal elements are also moved
for convenience, but we are looking for a theoritical bound). So the expected
bandwith is N * (N - 1) * sizeof(element) bytes.
https://stackoverflow.com/questions/29519222/how-to-transpose-a-16x16-matrix-using-simd-instructions
This answer states that a vectorized transpose can be done in n * log2(n)
(asymptotically) operations for any n with the assumption than vector-size = n

the general algorithm is described as follows:
permute n 32-bit rows
permute n 64-bit rows
...
permute n simd_width/2-bit rows

Not sure what it means exactly.
