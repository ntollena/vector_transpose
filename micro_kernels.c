#include "utils.h"
#include "micro_kernels.h"

void copy_sse_4x4(float* __restrict__ mat, float* __restrict__ matT, int i, int j) {
  __m128  r0, r1, r2, r3;
  int cst = i * MAT_SIZE + j;
  r0 = _mm_load_ps(&mat[cst]);
  r1 = _mm_load_ps(&mat[cst + 1 * MAT_SIZE]);
  r2 = _mm_load_ps(&mat[cst + 2 * MAT_SIZE]);
  r3 = _mm_load_ps(&mat[cst + 3 * MAT_SIZE]);
  int cst2 = j * MAT_SIZE + i;
  _mm_store_ps(&matT[j * MAT_SIZE + i], r0);
  _mm_store_ps(&matT[cst2 + 1 * MAT_SIZE], r1);
  _mm_store_ps(&matT[cst2 + 2 * MAT_SIZE], r2);
  _mm_store_ps(&matT[cst2 + 3 * MAT_SIZE], r3);
}
void trans_naive_out_bxb(float* __restrict__ mat, float* __restrict__ matT, int i, int j) {
  for (int ii = 0; ii < BLOCK_SIZE; ii ++) {
    for (int jj = 0; jj < BLOCK_SIZE; jj ++) {
      matT[(j + jj) * MAT_SIZE + i + ii] = mat[(i + ii) * MAT_SIZE + j + jj];
    }
  }
}

void copy_naive_out_bxb(float* __restrict__ mat, float* __restrict__ matT, int i, int j) {
  for (int ii = 0; ii < BLOCK_SIZE; ii ++) {
    for (int jj = 0; jj < BLOCK_SIZE; jj ++) {
      matT[(i + jj) * MAT_SIZE + j + ii] = mat[(i + ii) * MAT_SIZE + j + jj];
    }
  }
}

void trans_unpack_out_4x4_size(float* __restrict__ mat, float* __restrict__ matT, int i, int j, int size) {
  __m128  r0, r1, r2, r3;
  __m128  t0, t1, t2, t3;

  int cst = i * size + j;
  r0 = _mm_load_ps(&mat[cst + 0 * size]);
  r1 = _mm_load_ps(&mat[cst + 1 * size]);
  r2 = _mm_load_ps(&mat[cst + 2 * size]);
  r3 = _mm_load_ps(&mat[cst + 3 * size]);

  t0 = _mm_unpacklo_ps(r0, r2);
  t1 = _mm_unpackhi_ps(r0, r2);
  t2 = _mm_unpacklo_ps(r1, r3);
  t3 = _mm_unpackhi_ps(r1, r3);

  r0 = _mm_unpacklo_ps(t0, t2);
  r1 = _mm_unpackhi_ps(t0, t2);
  r2 = _mm_unpacklo_ps(t1, t3);
  r3 = _mm_unpackhi_ps(t1, t3);

  int cst2 = j * size + i;
  _mm_store_ps(&matT[cst2 + 0 * size], r0);
  _mm_store_ps(&matT[cst2 + 1 * size], r1);
  _mm_store_ps(&matT[cst2 + 2 * size], r2);
  _mm_store_ps(&matT[cst2 + 3 * size], r3);
}

void trans_unpack_out_4x4(float* __restrict__ mat, float* __restrict__ matT, int i, int j) {
  __m128  r0, r1, r2, r3;
  __m128  t0, t1, t2, t3;

  int cst = i * MAT_SIZE + j;
  r0 = _mm_load_ps(&mat[cst + 0 * MAT_SIZE]);
  r1 = _mm_load_ps(&mat[cst + 1 * MAT_SIZE]);
  r2 = _mm_load_ps(&mat[cst + 2 * MAT_SIZE]);
  r3 = _mm_load_ps(&mat[cst + 3 * MAT_SIZE]);

  t0 = _mm_unpacklo_ps(r0, r2);
  t1 = _mm_unpackhi_ps(r0, r2);
  t2 = _mm_unpacklo_ps(r1, r3);
  t3 = _mm_unpackhi_ps(r1, r3);

  r0 = _mm_unpacklo_ps(t0, t2);
  r1 = _mm_unpackhi_ps(t0, t2);
  r2 = _mm_unpacklo_ps(t1, t3);
  r3 = _mm_unpackhi_ps(t1, t3);

  int cst2 = j * MAT_SIZE + i;
  _mm_store_ps(&matT[cst2 + 0 * MAT_SIZE], r0);
  _mm_store_ps(&matT[cst2 + 1 * MAT_SIZE], r1);
  _mm_store_ps(&matT[cst2 + 2 * MAT_SIZE], r2);
  _mm_store_ps(&matT[cst2 + 3 * MAT_SIZE], r3);
}

#ifdef HAS_AVX
void trans_unpack_out_8x8_size(float* __restrict__ mat, float* __restrict__ matT, int i, int j, const int size) {
  __m256  r0, r1, r2, r3, r4, r5, r6, r7;
  __m256  t0, t1, t2, t3, t4, t5, t6, t7;

  int cst = i * size + j;
  r0 = _mm256_load_ps(&mat[cst + 0 * size]);
  r1 = _mm256_load_ps(&mat[cst + 1 * size]);
  r2 = _mm256_load_ps(&mat[cst + 2 * size]);
  r3 = _mm256_load_ps(&mat[cst + 3 * size]);
  r4 = _mm256_load_ps(&mat[cst + 4 * size]);
  r5 = _mm256_load_ps(&mat[cst + 5 * size]);
  r6 = _mm256_load_ps(&mat[cst + 6 * size]);
  r7 = _mm256_load_ps(&mat[cst + 7 * size]);

  t0 = _mm256_unpacklo_ps(r0, r1);
  t1 = _mm256_unpackhi_ps(r0, r1);
  t2 = _mm256_unpacklo_ps(r2, r3);
  t3 = _mm256_unpackhi_ps(r2, r3);
  t4 = _mm256_unpacklo_ps(r4, r5);
  t5 = _mm256_unpackhi_ps(r4, r5);
  t6 = _mm256_unpacklo_ps(r6, r7);
  t7 = _mm256_unpackhi_ps(r6, r7);

  r0 = _mm256_shuffle_ps(t0,t2,_MM_SHUFFLE(1,0,1,0));  
  r1 = _mm256_shuffle_ps(t0,t2,_MM_SHUFFLE(3,2,3,2));
  r2 = _mm256_shuffle_ps(t1,t3,_MM_SHUFFLE(1,0,1,0));
  r3 = _mm256_shuffle_ps(t1,t3,_MM_SHUFFLE(3,2,3,2));
  r4 = _mm256_shuffle_ps(t4,t6,_MM_SHUFFLE(1,0,1,0));
  r5 = _mm256_shuffle_ps(t4,t6,_MM_SHUFFLE(3,2,3,2));
  r6 = _mm256_shuffle_ps(t5,t7,_MM_SHUFFLE(1,0,1,0));
  r7 = _mm256_shuffle_ps(t5,t7,_MM_SHUFFLE(3,2,3,2));

  t0 = _mm256_permute2f128_ps(r0, r4, 0x20);
  t1 = _mm256_permute2f128_ps(r1, r5, 0x20);
  t2 = _mm256_permute2f128_ps(r2, r6, 0x20);
  t3 = _mm256_permute2f128_ps(r3, r7, 0x20);
  t4 = _mm256_permute2f128_ps(r0, r4, 0x31);
  t5 = _mm256_permute2f128_ps(r1, r5, 0x31);
  t6 = _mm256_permute2f128_ps(r2, r6, 0x31);
  t7 = _mm256_permute2f128_ps(r3, r7, 0x31);

  int cst2 = j * size + i;
  _mm256_store_ps(&matT[cst2 + 0 * size], t0);
  _mm256_store_ps(&matT[cst2 + 1 * size], t1);
  _mm256_store_ps(&matT[cst2 + 2 * size], t2);
  _mm256_store_ps(&matT[cst2 + 3 * size], t3);
  _mm256_store_ps(&matT[cst2 + 4 * size], t4);
  _mm256_store_ps(&matT[cst2 + 5 * size], t5);
  _mm256_store_ps(&matT[cst2 + 6 * size], t6);
  _mm256_store_ps(&matT[cst2 + 7 * size], t7);
}

void trans_blend_out_8x8(float* mat, float* matT, int i, int j) {
  __m256  r0, r1, r2, r3, r4, r5, r6, r7;
  __m256  t0, t1, t2, t3, t4, t5, t6, t7;

  int cst = i * MAT_SIZE + j;
  r0 = _mm256_insertf128_ps(_mm256_castps128_ps256(_mm_load_ps(&mat[cst + 0 * MAT_SIZE + 0])),
      _mm_load_ps(&mat[cst + 4 * MAT_SIZE + 0]), 1);
  r1 = _mm256_insertf128_ps(_mm256_castps128_ps256(_mm_load_ps(&mat[cst + 1 * MAT_SIZE + 0])),
      _mm_load_ps(&mat[cst + 5 * MAT_SIZE + 0]), 1);
  r2 = _mm256_insertf128_ps(_mm256_castps128_ps256(_mm_load_ps(&mat[cst + 2 * MAT_SIZE + 0])),
      _mm_load_ps(&mat[cst + 6 * MAT_SIZE + 0]), 1);
  r3 = _mm256_insertf128_ps(_mm256_castps128_ps256(_mm_load_ps(&mat[cst + 3 * MAT_SIZE + 0])),
      _mm_load_ps(&mat[cst + 7 * MAT_SIZE + 0]), 1);
  r4 = _mm256_insertf128_ps(_mm256_castps128_ps256(_mm_load_ps(&mat[cst + 0 * MAT_SIZE + 4])),
      _mm_load_ps(&mat[cst + 4 * MAT_SIZE + 4]), 1);
  r5 = _mm256_insertf128_ps(_mm256_castps128_ps256(_mm_load_ps(&mat[cst + 1 * MAT_SIZE + 4])),
      _mm_load_ps(&mat[cst + 5 * MAT_SIZE + 4]), 1);
  r6 = _mm256_insertf128_ps(_mm256_castps128_ps256(_mm_load_ps(&mat[cst + 2 * MAT_SIZE + 4])),
      _mm_load_ps(&mat[cst + 6 * MAT_SIZE + 4]), 1);
  r7 = _mm256_insertf128_ps(_mm256_castps128_ps256(_mm_load_ps(&mat[cst + 3 * MAT_SIZE + 4])),
      _mm_load_ps(&mat[cst + 7 * MAT_SIZE + 4]), 1);

  t0 = _mm256_unpacklo_ps(r0,r1);
  t1 = _mm256_unpackhi_ps(r0,r1);
  t2 = _mm256_unpacklo_ps(r2,r3);
  t3 = _mm256_unpackhi_ps(r2,r3);
  t4 = _mm256_unpacklo_ps(r4,r5);
  t5 = _mm256_unpackhi_ps(r4,r5);
  t6 = _mm256_unpacklo_ps(r6,r7);
  t7 = _mm256_unpackhi_ps(r6,r7);

  __m256 v;

  v = _mm256_shuffle_ps(t0,t2, 0x4E);
  r0 = _mm256_blend_ps(t0, v, 0xCC);
  r1 = _mm256_blend_ps(t2, v, 0x33);

  v = _mm256_shuffle_ps(t1,t3, 0x4E);
  r2 = _mm256_blend_ps(t1, v, 0xCC);
  r3 = _mm256_blend_ps(t3, v, 0x33);

  v = _mm256_shuffle_ps(t4,t6, 0x4E);
  r4 = _mm256_blend_ps(t4, v, 0xCC);
  r5 = _mm256_blend_ps(t6, v, 0x33);

  v = _mm256_shuffle_ps(t5,t7, 0x4E);
  r6 = _mm256_blend_ps(t5, v, 0xCC);
  r7 = _mm256_blend_ps(t7, v, 0x33);

  int cst2 = j * MAT_SIZE + i;
  _mm256_store_ps(&matT[cst2 + 0 * MAT_SIZE], r0);
  _mm256_store_ps(&matT[cst2 + 1 * MAT_SIZE], r1);
  _mm256_store_ps(&matT[cst2 + 2 * MAT_SIZE], r2);
  _mm256_store_ps(&matT[cst2 + 3 * MAT_SIZE], r3);
  _mm256_store_ps(&matT[cst2 + 4 * MAT_SIZE], r4);
  _mm256_store_ps(&matT[cst2 + 5 * MAT_SIZE], r5);
  _mm256_store_ps(&matT[cst2 + 6 * MAT_SIZE], r6);
  _mm256_store_ps(&matT[cst2 + 7 * MAT_SIZE], r7);
}

void copy_contiguous_avx_8x8(float* __restrict__ mat, float* __restrict__ matT, int i, int j) {
  __m256  r0, r1, r2, r3, r4, r5, r6, r7;
  int cst = i * MAT_SIZE + j;
  r0 = _mm256_load_ps(&mat[cst]);
  r1 = _mm256_load_ps(&mat[cst + 1 * MAT_SIZE]);
  r2 = _mm256_load_ps(&mat[cst + 2 * MAT_SIZE]);
  r3 = _mm256_load_ps(&mat[cst + 3 * MAT_SIZE]);
  r4 = _mm256_load_ps(&mat[cst + 4 * MAT_SIZE]);
  r5 = _mm256_load_ps(&mat[cst + 5 * MAT_SIZE]);
  r6 = _mm256_load_ps(&mat[cst + 6 * MAT_SIZE]);
  r7 = _mm256_load_ps(&mat[cst + 7 * MAT_SIZE]);
  _mm256_store_ps(&matT[i * MAT_SIZE + j], r0);
  _mm256_store_ps(&matT[cst + 1 * MAT_SIZE], r1);
  _mm256_store_ps(&matT[cst + 2 * MAT_SIZE], r2);
  _mm256_store_ps(&matT[cst + 3 * MAT_SIZE], r3);
  _mm256_store_ps(&matT[cst + 4 * MAT_SIZE], r4);
  _mm256_store_ps(&matT[cst + 5 * MAT_SIZE], r5);
  _mm256_store_ps(&matT[cst + 6 * MAT_SIZE], r6);
  _mm256_store_ps(&matT[cst + 7 * MAT_SIZE], r7);
}

void copy_avx_8x8(float* __restrict__ mat, float* __restrict__ matT, int i, int j) {
  __m256  r0, r1, r2, r3, r4, r5, r6, r7;
  int cst = i * MAT_SIZE + j;
  r0 = _mm256_load_ps(&mat[cst]);
  r1 = _mm256_load_ps(&mat[cst + 1 * MAT_SIZE ]);
  r2 = _mm256_load_ps(&mat[cst + 2 * MAT_SIZE ]);
  r3 = _mm256_load_ps(&mat[cst + 3 * MAT_SIZE ]);
  r4 = _mm256_load_ps(&mat[cst + 4 * MAT_SIZE ]);
  r5 = _mm256_load_ps(&mat[cst + 5 * MAT_SIZE ]);
  r6 = _mm256_load_ps(&mat[cst + 6 * MAT_SIZE ]);
  r7 = _mm256_load_ps(&mat[cst + 7 * MAT_SIZE ]);
  cst = j * MAT_SIZE + i;
  _mm256_store_ps(&matT[cst], r0);
  _mm256_store_ps(&matT[ cst + 1 * MAT_SIZE ], r1);
  _mm256_store_ps(&matT[ cst + 2 * MAT_SIZE ], r2);
  _mm256_store_ps(&matT[ cst + 3 * MAT_SIZE ], r3);
  _mm256_store_ps(&matT[ cst + 4 * MAT_SIZE ], r4);
  _mm256_store_ps(&matT[ cst + 5 * MAT_SIZE ], r5);
  _mm256_store_ps(&matT[ cst + 6 * MAT_SIZE ], r6);
  _mm256_store_ps(&matT[ cst + 7 * MAT_SIZE ], r7);
}
#endif
